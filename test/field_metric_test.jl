@testset "Field Metric Tests" begin
  rtol = 1.0E-6 
  vmecfile = joinpath(@__DIR__,"wout_ku4.nc")
  vmec = VMEC.readVmecWout(vmecfile)
  vmecsurf = VmecSurface(0.9, vmec)
  @test isapprox(∇B_double_dot_∇B_target(vmecsurf, 128, 128), 5.154790731388864, rtol=rtol)
  @test isapprox(L∇B_target(vmecsurf, 128, 128), 1.104919430736065, rtol=rtol)
end
