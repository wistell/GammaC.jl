using StellaratorOptimizationMetrics
using Test
using Interpolations
using LinearAlgebra
using Roots
using DoubleExponentialFormulas
using PlasmaEquilibriumToolkit
using VMEC

include("field_metric_test.jl")
include("BallooningStabilityTests.jl")
include("gammac_tests.jl")
