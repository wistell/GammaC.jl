## Interfaces
```@autodocs
Modules = [StellaratorOptimizationMetrics.GammaC]
Public = false
Pages = ["Interfaces.jl"]
```

## Functions
```@autodocs
Modules = [StellaratorOptimizationMetrics.GammaC]
Public = false
Pages = ["Functions.jl"]
```
