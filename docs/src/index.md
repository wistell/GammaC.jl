```@meta
CurrentModule = StellaratorOptimizationMetrics
```

# GammaC

Documentation for [GammaC](https://gitlab.com/wistell/StellaratorOptimizationMetrics.jl).

```@index
Pages = ["overview.md"]
```
The GammaC code is used to evaluate the ``\\Gamma_c`` function found in Nemov PoP 2008, "Poloidal motion of trapped particle orbits in real-space coordinates" 
This metric is used to align contours of the second adiabatic invariant ``J_\\parallel = \\int v_\\parallel ds`` with flux surfaces. 
The metric has been used to optimized QH configurations for energetic particle confinement with some success.

# Calling the Code

Calling the GammaC code will first require access to VMEC objects from the PET library. There are multiple options available to call the code depending on whether you want to use multiple surfaces or a single surface, and whether you want to use MPI capabilities. In all cases a vmec wout file is needed

The following example will calculate and print ``\\Gamma_c`` (and ``\\epsilon_\\mathrm{eff}``) on a single surface without MPI.  It assumes you have the PlasmaEquilibriumToolkit, VMEC and GammaC packages installed. 

```julia
s = 0.5 #normalized toroidal flux to calculate on
phiBegin = 0.0 #starting toroidal angle value
phiEnd = 400.0 #ending toroidal angle value
phiStep = 2.5e-3 #resolution in the toroidal angle step size
BResolution = 200 #resolution in the magnetic field steps

using PlasmaEquilibriumToolkit, VMEC, StellaratorOptimizationMetric
wout = "wout_file.nc";
vmec = readVmecWout(wout);
vmec_surf = VmecSurface(s,vmec);
println(StellOptMetrics.gamma_c_target(vmec_surf, phiBegin:phiStep:phiEnd, BResolution))
```

Additional options for calling the code are presented below

# The Calculation

The calculation of ``\\Gamma_c`` is done in several steps.  These are:

1) Generate a spline of ``|B|`` from the input beginning ``\\phi`` (toroidal angle) value to the end ``\\phi`` value with knot placements given by the input resolution
2) Calculate all the maxima and minima of ``|B|``
3) Loop over magnetic field values, ``B_t``, between the minimum and maximum value on the flux surface, with the resolution given by the input BResolution
4) Calculate all points where ``|B| = B_t`` thus identifying all the wells
5) Calculate all the needed quantities for ``\\Gamma_c`` and ``\\epsilon_\\mathrm{eff}`` for each well (see eqs 50 and 51 in Nemov)
6) Sum the quantities over all the wells
7) Integrate over all ``B_t`` values
8) Divide by the normalization integrals

# Alternative call options

To call with multiple surfaces (assume the variable designations are already made as above)

```julia
sarray = [0.5, 0.6]
wout = "wout_file.nc";
vmec = readVmecWout(wout);
println(GammaC.GammacTarget(sarray, vmec, phiBegin:phiStep:phiEnd, BResolution))
```

To call with MPI (using the single surface format, but multiple surfaces is also possible)
```julia
using MPI
MPI.Init()
comm = MPI.COMM_WORLD

wout = NetCDF.open("wout_file.nc");
vmec, vmec_data = readVmecWout(wout);
vmec_surf = VmecSurface(s,vmec);
println(GammaC.GammacTarget(vmec_surf, phiBegin:phiStep:phiEnd, BResolution, comm))
```
