function summodosd(surf::S, init_zeta::Float64, init_theta::Float64, npt::Int64, x::Vector{Float64},
                   c2::Vector{Float64}, k_s::Vector{Float64}, bsuppar::Vector{Float64}, b2::Vector{Float64}
                   ) where {S <: AbstractSurface}


  lfail_balloon = false;
  #it appears that the lambdas are on the half-grid and iotas are on the full grid.  seems to be a bug
  #We'll just assume full grid
  s = surf.s
  zetacn  = init_zeta;
  zetang = zetacn .+ x;

  #-----------------------------------------
  #        Begin Fourier inversion
  #
  #
  # Aaron's version using PET
  #      Initialize for Fourier inversion
  bfield   = zeros(npt);
  bfields  = zeros(npt);
  bfieldth = zeros(npt);
  bfieldze = zeros(npt);
  rboo     = zeros(npt);
  rs       = zeros(npt);
  rth      = zeros(npt);
  rze      = zeros(npt);
  zs       = zeros(npt);
  zth      = zeros(npt);
  zze      = zeros(npt);
  lambdas  = zeros(npt);
  lambdaze = zeros(npt);
  lambdath = zeros(npt);
  bsupth   = zeros(npt);
  bsupze   = zeros(npt);
  thetang  = zeros(npt);
  ψpest = -surf.phi[1]/2/π
  #determine what kind of coordinates we need
  internal_coords = InternalFromPest()(PestCoordinates(ψpest, 0.0, 0.0), surf)
  ctype = typeof(internal_coords)
  #generate the new coordinates
  ic = ctype(surf.s, init_theta, init_zeta)
  pestCoords = PestFromInternal()(ic, surf)
  αpest = pestCoords.α

  
  for i in 1:length(zetang)
    ζ = zetang[i]
    #sign convention used in fortran code
    pestCoords  = PestCoordinates(ψpest, αpest, -1*ζ);
    internal_coords  = InternalFromPest()(pestCoords,surf)

    thetang[i]  = internal_coords.θ
    bfield[i]   = surface_get(internal_coords, surf, :b)
    bfields[i]  = surface_get(internal_coords, surf, :b, deriv=:ds)
    bfieldth[i] = surface_get(internal_coords, surf, :b, deriv=:dθ)
    bfieldze[i] = surface_get(internal_coords, surf, :b, deriv=:dζ)
    rboo[i]     = surface_get(internal_coords, surf, :r)
    rs[i]       = surface_get(internal_coords, surf, :r, deriv=:ds)
    rth[i]      = surface_get(internal_coords, surf, :r, deriv=:dθ)
    rze[i]      = surface_get(internal_coords, surf, :r, deriv=:dζ)
    zs[i]       = surface_get(internal_coords, surf, :z, deriv=:ds)
    zth[i]      = surface_get(internal_coords, surf, :z, deriv=:dθ)
    zze[i]      = surface_get(internal_coords, surf, :z, deriv=:dζ)
    lambdas[i]  = surface_get(internal_coords, surf, :λ, deriv=:ds)
    lambdaze[i] = surface_get(internal_coords, surf, :λ, deriv=:dζ)
    lambdath[i] = surface_get(internal_coords, surf, :λ, deriv=:dθ)
    bsupth[i]   = surface_get(internal_coords, surf, :bsupu)
    bsupze[i]   = surface_get(internal_coords, surf, :bsupv)
  end


  #   End Fourier inversion
  #-----------------------------------------

  #      auxiliary quantities

  rboo2 = rboo.^2;
  bfieldi = 1 ./ bfield;
  b2 .= bfield.^2;
  bfield2i = bfieldi.^2;

  #      lower metric elements (gᵢⱼ = ∇rᵢᵏ δₖₗ ∇rⱼˡ)
  gtssub = rth.*rs + zth.*zs;
  gstsub = gtssub;
  gzssub = rze.*rs + zze.*zs;
  gszsub = gzssub;
  gtzsub = rth.*rze + zth.*zze;
  gztsub = gtzsub;
  gttsub = (rth.^2) + (zth.^2);
  gsssub = (rs.^2) + (zs.^2);
  gzzsub = (rze.^2) + (zze.^2) + rboo2;

  #       jacobian (jacob2 = det(g))

  jacob2 = gsssub.*gttsub.*gzzsub +
           2*gstsub.*gtzsub.*gszsub -
           gttsub.*gszsub.^2 -
           gzzsub.*gstsub.^2 -
           gsssub.*gztsub.^2;
  rjac2i = 1 ./ jacob2;

  #      upper metric elements ( (g⁻¹)ⁱʲ )
  #      ... Why are we using Kramer's rule?
  gsssup = (gttsub.*gzzsub - gtzsub.*gztsub).*rjac2i;
  gttsup = (gsssub.*gzzsub - gszsub.*gzssub).*rjac2i;
  gzzsup = (gsssub.*gttsub - gstsub.*gtssub).*rjac2i;
  gstsup = (gztsub.*gzssub - gstsub.*gzzsub).*rjac2i;
  gtssup = gstsup;
  gszsup = (gtssub.*gtzsub - gttsub.*gzssub).*rjac2i;
  gzssup = gszsup;
  gtzsup = (gstsub.*gzssub - gsssub.*gztsub).*rjac2i;
  gztsup = gtzsup;

  #      covariant B-components (Bᵢ = gᵢⱼ bʲ)
  bsubs =  bsupze.*gszsub + bsupth.*gstsub;
  bsubth =  bsupze.*gtzsub + bsupth.*gttsub;
  bsubze =  bsupze.*gzzsub + bsupth.*gtzsub;

  #      covariant curvature components (κⁱ = μ₀∇pⁱ/B²  + ∇⟂|B|/|B|, ∇⟂|B| = ∇|B| - (B⋅∇|B|)B/|B|² )
  aux = (bsupze.*bfieldze + bsupth.*bfieldth).*bfield2i;
  μ0 = 4*π*1.0E-7
  cks = bfieldi.*(bfields + bfieldi.*surf.pres[2]*μ0 - bsubs.*aux);
  ckth = bfieldi.*(bfieldth - bsubth.*aux);
  # ckϕ = bfieldi.*(bfieldze - bsubze.*aux) (not needed for the upcoming pushforward)
  # println("ζ = $(zetang[1]), θ = $init_theta, cks = $cks, ckth = $ckth")

  #      normal curvature in (s,alpha,phi)-coordinates ( ∇S/kα = ∇α + q'θₖ∇ψ ( +∇λ b/c of VMEC nastiness))
  lam1 = 1 .+ lambdath;
  lam2 = - surf.iota[1] .+ lambdaze;
  lam3 = - surf.iota[2]*x .+ lambdas;
  k_s .= cks - ckth.*lam3 ./ lam1;
  # println("s ζ = $(zetang[1]), θ = $(thetang[1]), ∇S1 = $([lam3[1], lam1[1], lam2[1]])")

  #      normal vector squared ‖∇S/kα‖² (Equal to B²(1+Λ²)/‖∇ψ‖² )
  c2 .= gzzsup.*lam2.^2 + gttsup.*lam1.^2 +
        gsssup.*lam3.^2 + 2*lam2.*lam3.*gzssup +
        2*lam3.*lam1.*gtssup + 2*lam1.*lam2.*gztsup;

  #      Contravariant zeta-component of field going to B.grad (B^ζ = (∇α×∇ψ)⋅∇ζ = 1/J)
  bsuppar .= bsupze;

#  for i in 1:length(zetang)
#    println("comp: ",zetang[i]," ",thetang[i], " ",bfield[i], " ",bfields[i], " ",bfieldth[i],
#            " ",bfieldze[i]," ",rboo[i]," ",rs[i]," ",rth[i]," ",rze[i]," ",
#            zs[i]," ",zth[i]," ",zze[i]," ",lambdas[i]," ",lambdaze[i]," ",lambdath[i]," ",
#            bsupth[i]," ",bsupze[i]," ",cks[i]," ",aux[i]," ",c2[i])
#  end

  return lfail_balloon
end


# Gets the coefficients for interpolation by a BallooningCoefInterp object
# Returns c2_coef, κ_coef, and bsuppar
# To get c2 and ks (as the above function does), one uses
#    c2 = c2_coef[:, :, 1] + c2_coef[:, :, 2] * X + c2_coef[:, :, 3] * X^2
#    ks =  κ_coef[:, :, 1] +  κ_coef[:, :, 2] * X
# where X is the distance from the "origin" of the eigenvalue problem
function ballooning_coefficient_grid(surf::S, ζvec::AbstractVector, 
                                     αvec::AbstractVector
                                    ) where {S <: AbstractSurface}
  μ0 = 4*π*1.0E-7
  s = surf.s
  Nζ = length(ζvec);
  Nα = length(αvec);

  c2_coefs  = zeros(Nα, Nζ, 3);
  κ_coefs   = zeros(Nα, Nζ, 2);
  bsuppar   = zeros(Nα, Nζ)

  for iα = 1:length(αvec)
    αpest = αvec[iα]
    ψpest = -surf.phi[1]/2/π
    for iζ in 1:length(ζvec)
      ζ = ζvec[iζ]
      #sign convention used in fortran code
      pestCoords  = PestCoordinates(ψpest, αpest, -1*ζ);
      internal_coords  = InternalFromPest()(pestCoords,surf)

      bmag     = surface_get(internal_coords, surf, :b)
      dbmag = zeros(3);
      dbmag[1] = surface_get(internal_coords, surf, :b, deriv=:ds)
      dbmag[2] = surface_get(internal_coords, surf, :b, deriv=:dθ)
      dbmag[3] = surface_get(internal_coords, surf, :b, deriv=:dζ)

      dr = zeros(3,3);
      dr[1, 1] = surface_get(internal_coords, surf, :r, deriv=:ds)
      dr[1, 2] = surface_get(internal_coords, surf, :r, deriv=:dθ)
      dr[1, 3] = surface_get(internal_coords, surf, :r, deriv=:dζ)
      dr[2, 1] = surface_get(internal_coords, surf, :z, deriv=:ds)
      dr[2, 2] = surface_get(internal_coords, surf, :z, deriv=:dθ)
      dr[2, 3] = surface_get(internal_coords, surf, :z, deriv=:dζ)
      dr[3, 3] = surface_get(internal_coords, surf, :r)

      dλ = zeros(3);
      dλ[1] = surface_get(internal_coords, surf, :λ, deriv=:ds)
      dλ[2] = surface_get(internal_coords, surf, :λ, deriv=:dθ)
      dλ[3] = surface_get(internal_coords, surf, :λ, deriv=:dζ)

      bsup = zeros(3);
      bsup[2] = surface_get(internal_coords, surf, :bsupu)
      bsup[3] = surface_get(internal_coords, surf, :bsupv)

      gsub = dr'*dr;
      bsub = gsub*bsup;

      ∇p = [surf.pres[2], 0.0, 0.0];
      κsub = μ0.*∇p./(bmag^2) + dbmag./bmag - ((dbmag'*bsup)/bmag^3).*bsub

      ι  = surf.iota[1];
      ιs = surf.iota[2];
      ∇S1 = dλ + [0, 1, -ι];
      # ∇S2 =      [0, 0, -ιs];

      gsup = inv(gsub);
      c2_coefs[iα, iζ, 1] =     ∇S1'  * gsup       * ∇S1   / (bmag^2);
      c2_coefs[iα, iζ, 2] = 2 * ∇S1'  * gsup[:, 1] * (-ιs) / (bmag^2);
      c2_coefs[iα, iζ, 3] =     (-ιs) * gsup[1, 1] * (-ιs) / (bmag^2);

#     k_s .= cks - ckth.*lam3 ./ lam1;
      κ_coefs[iα, iζ, 1] = κsub[1] - κsub[2] * ∇S1[1] / ∇S1[2]
      κ_coefs[iα, iζ, 2] =         - κsub[2] * (-ιs)  / ∇S1[2]

      bsuppar[iα, iζ] = bsup[3];
    end
  end
  
  return c2_coefs, κ_coefs, bsuppar
end
