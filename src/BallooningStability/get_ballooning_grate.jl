function get_ballooning_grate(surf::S,
                              init_zeta::Float64, init_theta::Float64,
                              params::BalloonParams, bci::BallooningCoefInterp,
                              interp::Bool, np0_in::Integer
                              ) where {S <: AbstractSurface}
  # Frequently used values as local variables
  # np0_in = 793;   # coarsest grid used in Richardson's loop
  s = surf.s

  #Calculate limits of integration
  # account for K_w potential wells
  xmax = π*(2*params.k_w - 1) ./ (2*surf.nfp*surf.iota[1])
  np  = Int(6 * floor(np0_in / 6) + 1);      # number of points always odd and multiple of 3
  xmin = -xmax;
  h   = 2*(xmax/(np - 1));

  # Create vectors for coefficients and diagonals
  coeffs_f = coeffs_struct(np)
  coeffs_h = coeffs_struct(np-1);

  lfail_balloon, A = getmatrix(surf, s, init_zeta, init_theta, xmin,
                                h, np, coeffs_f, coeffs_h,
                                params, bci, interp);

  if lfail_balloon      # if theta does not converge in getmatrix/coeffs/summodosd/get_theta
    println("get_ballooning_grate: getmatrix failed"); flush(stdout);
    return
  end

  eigm = eigmax(A); # only need eigenvalue

  # if eigm < 0
  #   growthRate = -sqrt(-eigm);
  # else
  #   growthRate = sqrt(eigm);
  # end
  # return growthRate, xmax, np, 1
  return eigm, xmax
end
