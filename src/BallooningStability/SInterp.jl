function spline_kernel(αs, ζs, A)
  itp = interpolate(A, (BSpline(Cubic(Periodic(OnCell()))), BSpline(Cubic(Line(OnGrid())))))

  # Below is a hacky solution for Interpolations.jl not having support for
  # Hessians for extrapolation objects. Otherwise, I would use their automatic
  # periodic function extrapolation.
  buf = 2
  Nα = length(αs)
  Nζ = length(ζs)
  periodic_coefs = Interpolations.OffsetMatrix(vcat(itp.coefs.parent[end-buf+1:end, :],
                                                    itp.coefs.parent,
                                                    itp.coefs.parent[1:buf, :]), itp.coefs.offsets)

  itp = Interpolations.BSplineInterpolation(periodic_coefs,
                                            itp.it,
                                            (Base.OneTo(Nα+2buf),Base.OneTo(Nζ)));

  αmax = αs[end];
  αmin = αs[1];
  hα = (αmax - αmin)/(Nα-1);
  αnew = LinRange(αmin - buf*hα, αmax + buf*hα, Nα+2buf)
  sitp = scale(itp, αnew , ζs)

  return sitp
end

## Get an interpolant of a surface.
#   Nζ is the degree of the polynomial interpolant minus 1
#   Nα is the degree of the Fourier interpolant
#   specify c2_nodes, κ_nodes, bsuppar_nodes only if you already have the
#     coefficients on a grid (used in adaptive method)
function SInterp(surf::S, Nζ::Integer, Nα::Integer;
                 c2_nodes=[], κ_nodes = [], bsuppar_nodes = []
                ) where {S <: AbstractSurface}
  nfp = surf.nfp;
  iota = surf.iota[1];

  ζs = (2π/nfp) .* (0:Nζ)./Nζ;
  αs = (0:Nα-1) .* (2π/Nα);

  if c2_nodes == []
    c2_nodes, κ_nodes, bsuppar_nodes = ballooning_coefficient_grid(surf, ζs, αs)
  end


  c2 = Vector{Interpolations.ScaledInterpolation}(undef, 3);
  for ii = 1:3
    c2[ii] = spline_kernel(αs, ζs, c2_nodes[:, :, ii]);
  end

  κ = Vector{Interpolations.ScaledInterpolation}(undef, 2);
  for ii = 1:2
    κ[ii] = spline_kernel(αs, ζs, κ_nodes[:, :, ii]);
  end

  bsuppar = [spline_kernel(αs, ζs,  bsuppar_nodes)];

  return SInterp(c2, κ, bsuppar, nfp, iota)
end

function Sscale!(itp, val)
  itp.itp.coefs[:] = itp.itp.coefs[:] .* val
end

function scale_bci(bci::SInterp, surf::S, params::BalloonParams
                  ) where {S <: AbstractSurface}
  phipc = surf.phi[2]/(-2*π) #phi derivative
  μ0 = 4*π*1.0e-7
  prespn = 2*surf.pres[2]*μ0/(params.beta0*params.b0_v^2);     # pressure normalized to p_0
  phipn2 = (phipc/(params.b0_v*params.amin^2))^2;      # perpend. lengths normalized to a
  eps2 = (params.r0/params.amin)^2;
  factor = eps2*params.beta0*prespn/phipn2;

  for itp in bci.c2;
    Sscale!(itp, (params.amin^2) * (params.b0_v^2));
  end

  for itp in bci.κ
    Sscale!(itp, factor);
  end

  for itp in bci.bsuppar
    Sscale!(itp, params.r0 / params.b0_v);
  end
end


function get_c2(bci::SInterp)
  return bci.c2
end

function get_κ(bci::SInterp)
  return bci.κ
end

function get_bsuppar(bci::SInterp)
  return bci.bsuppar
end

function eval_kernel(bci::SInterp, itp, α, ζ)
  return itp(mod.(α, 2π),ζ)
end

## eval_coefs:
# Get the values of c2_coefs, κ_coefs, and bsuppar_coefs on a grid defined
# by α and ζ
function eval_coefs(bci::SInterp, α::AbstractVector, ζ::AbstractVector;
                    eval_c2::Bool=true, eval_κ::Bool=true, eval_bsuppar::Bool=true)
  nα = length(α);
  nζ = length(ζ);

  c2_nodes = []; κ_nodes = []; bsuppar_nodes = [];

  if eval_c2
    c2_nodes = zeros(nα, nζ, 3)
    c2 = get_c2(bci);
    for ii = 1:3
      c2_nodes[:,:,ii] = eval_kernel(bci, c2[ii], α, ζ)
    end
  end

  if eval_κ
    κ_nodes  = zeros(nα, nζ, 2)
    κ = get_κ(bci);
    for ii = 1:2
      κ_nodes[:,:,ii] = eval_kernel(bci, κ[ii], α, ζ)
    end
  end

  if eval_bsuppar
    bsuppar = get_bsuppar(bci);
    bsuppar_nodes = eval_kernel(bci, bsuppar[1], α, ζ)
  end

  return c2_nodes, κ_nodes, bsuppar_nodes
end


function S_ddeval_kernel(itp, α, ζ)
  # TODO: should add a check that ζ is in range
  n1 = length(α); n2 = length(ζ);
  # f = zeros(n1, n2)
  # df = zeros(n1, n2, 2);
  # ddf = zeros(n1, n2, 2, 2);

  f = zeros(n1, n2)
  df = zeros(n1, n2, 2);
  ddf = zeros(n1, n2, 2, 2);


  α = mod.(α, 2π)

  for (ii, αi) = enumerate(α), (jj, ζi) = enumerate(ζ)
    f[ii,jj]       = itp(αi, ζi)
    df[ii,jj,:]    = Interpolations.gradient(itp, αi, ζi)
    ddf[ii,jj,:,:] = Interpolations.hessian(itp, αi, ζi)
  end

  return f, df, ddf
end

## ddeval_coefs:
# Get the values of c2_coefs, κ_coefs, and bsuppar_coefs and their derivatives
# w.r.t. α and ζ on a grid defined by α and ζ
function ddeval_coefs(bci::SInterp, α::AbstractVector, ζ::AbstractVector;
                      eval_c2::Bool=true, eval_κ::Bool=true, eval_bsuppar::Bool=true)
  nfp = get_nfp(bci);
  L = 2π/nfp;

  nα = length(α);
  nζ = length(ζ);

  c2, dc2, ddc2 = [], [], []
  if eval_c2
    c2 = zeros(nα, nζ, 3)
    dc2 = zeros(nα, nζ, 2, 3)
    ddc2 = zeros(nα, nζ, 2, 2, 3)
    c2_coef = get_c2(bci);
    for ii = 1:3
      c2[:,:,ii], dc2[:,:,:,ii], ddc2[:,:,:,:,ii] = S_ddeval_kernel(c2_coef[ii], α, ζ);
    end
  end

  κ, dκ, ddκ = [], [], []
  if eval_κ
    κ  = zeros(nα, nζ, 2)
    dκ = zeros(nα, nζ, 2, 2)
    ddκ = zeros(nα, nζ, 2, 2, 2)
    κ_coef = get_κ(bci);
    for ii = 1:2
      κ[:,:,ii], dκ[:,:,:,ii], ddκ[:,:,:,:,ii] = S_ddeval_kernel(κ_coef[ii], α, ζ);
    end
  end

  bsuppar, dbsuppar, ddbsuppar = [], [], []
  if eval_bsuppar
    bsuppar_coef = get_bsuppar(bci);
    bsuppar, dbsuppar, ddbsuppar = S_ddeval_kernel(bsuppar_coef[1], α, ζ);
  end

  return c2, κ, bsuppar, dc2, dκ, dbsuppar, ddc2, ddκ, ddbsuppar
end

function S_refinement_init()
  return 20, 10
end

function S_refinement_grid(Nα, Nζ, nfp)
  αvec = (0:Nα-1) .* (2π/Nα);
  ζs = (0:Nζ) .* (2π/(nfp*Nζ))

  return αvec, ζs
end

function S_refinement_increment(Nα, Nζ)
  return 2Nα, 2Nζ
end
