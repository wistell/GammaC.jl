
struct coeffs_struct
  x::Vector{Float64}
  p::Vector{Float64}
  q::Vector{Float64}
  r::Vector{Float64}

  function coeffs_struct(np::Int64)
    return new(zeros(np), zeros(np), zeros(np), zeros(np))
  end
end

struct ddcoeffs_struct
  x::Vector{Float64}

  p::Vector{Float64}
  q::Vector{Float64}
  r::Vector{Float64}

  dp::AbstractArray
  dq::AbstractArray
  dr::AbstractArray

  ddp::AbstractArray
  ddq::AbstractArray
  ddr::AbstractArray

  function ddcoeffs_struct(np::Int64)
    return new(zeros(np),
               zeros(np), zeros(np), zeros(np),
               zeros(np,2), zeros(np,2), zeros(np,2),
               zeros(np,2,2), zeros(np,2,2), zeros(np,2,2))
  end
end


struct BalloonParams
  k_w::Int64
  l_geom_input::Bool
  l_tokamak_input::Bool
  kth::Int64

  r0::Float64
  amin::Float64
  beta0::Float64
  b0_v::Float64
end


abstract type BallooningCoefInterp end

# Chebyshev x Fourier interpolation of the ballooning eigenvalue problem coefficients in Clebsch x Toroidal coordinates
struct FCInterp <: BallooningCoefInterp
  c2     ::AbstractArray;
  κ      ::AbstractArray;
  bsuppar::AbstractArray;
  nfp::Integer;
  iota::Number;
end

# spline interpolation of the ballooning eigenvalue problem coefficients in Clebsch x Toroidal coordinates
struct SInterp <: BallooningCoefInterp
  c2     ::AbstractArray;
  κ      ::AbstractArray;
  bsuppar::AbstractArray;

  nfp::Integer;
  iota::Number;
end


"""
    cobra(eq, surfs, nwells, θs, ζs, mode=1, geom_input=true, tokamak_input=false, lscreen=true)

Calculates ballooning equilibrium based off of the CobraVmec code. Originally converted from fortran to Julia by Sanket Patil, rewritten by Aaron Bader

# Arguments
 - `eq::E`: A Vmec or DESC (eventually) equilibrium
 - `slist::Vector{Int64}`: list of s values
 - `nwells::Int64`: Number of helical wells to include
# Optional inputs
 - `θs::Vector{Float64}`: Vector of starting θ values (in radians)
 - `ζs::Vector{Float64}`: Vector of starting ζ values (in radians normalized to field periods)
 - `mode::Int64`: mode number, 1 is the most unstable mode
 - `geom_input::Bool`: Flag, default true
 - `tokamak_input::Bool`: Flag, default false, not sure we'll ever use this
 - `lscreen::Bool`: Flag, output to screen, default true

"""
function cobra(eq::E, slist::AbstractVector, nwells::Int64;
               θs::AbstractVector = [0.0, π/2, π, 2*π/3, 2*π],
               ζs::AbstractVector = [0.0, π/8, π/4, 2*π/8, π/2],
               mode::Int64=1, geom_input::Bool= true, tokamak_input::Bool=false,
               lscreen::Bool=false, print_progress::Bool=false,
               interp::Bool=true, reltol::Float64=1e-4, np::Int64=793,
               spline::Bool=true) where {E <: AbstractMagneticEquilibrium}

  #this will still crash if surfaces are too small, can determine minimum value
  #but better to allow extrapolation
  slist = slist[slist .> 0.001]
  slist = slist[slist .< 0.999]
  ns = length(slist)

  # Add here:  date and time if wanted
  surf = magnetic_surface(slist[1], eq)
  balloonGrowth = zeros(length(slist), length(θs), length(ζs))

  for (i_s,s) in enumerate(slist)
    if print_progress
      println("i_s = $(i_s)/$(length(slist)), ι = $(surf.iota[1])"); flush(stdout);
    end
    
    surf = magnetic_surface(s, eq)
    bg = cobra(surf, nwells, θs=θs, ζs=ζs,mode=mode,
               geom_input=geom_input, tokamak_input=tokamak_input,
               print_progress=print_progress, interp=interp, reltol=reltol,
               np=np, spline=spline)
    balloonGrowth[i_s, :, :] = bg
  end
  return balloonGrowth
end


function cobra(surf::S, nwells::Int64;
               θs::AbstractVector = [0.0, π/2, π, 2*π/3, 2*π],
               ζs::AbstractVector = [0.0, π/8, π/4, 2*π/8, π/2],
               mode::Int64=1, geom_input::Bool= true, tokamak_input::Bool=false,
               print_progress::Bool=false,
               interp::Bool=true, reltol::Float64=1e-4, np::Int64=793,
               spline::Bool=true) where {S <: AbstractSurface}
  #=
  _____________________________________________________________________________________
  |                                                                                   |
  |              COBRA (COde for Ballooning Rapid Analysis)                           |
  |                     ==       =          =     =                                   |
  |                                                                                   |
  |      VERSION: julia rewrite of                                                    |
  |               4.1  (FORTRAN 90: fixed FORMAT; standard I/O; VMEC coordinates)     |
  |                    (Accepts ASYMMETRIC input from VMEC via LASYM)                 |
  |                    (Accepts initial line location as geometrical or (ALHPA,ANGLE) |
  |                    (Good for LRFP=T or F)                                         |
  |      Last update: 03/27/13                                                        |
  |                                                                                   |
  |      AUTHORS:   R. Sanchez                                                        |
  |             Universidad Carlos III de Madrid, Leganes 28911, SPAIN.               |
  |                 S.P. Hirshman                                                     |
  |             Oak Ridge National Laboratory, Oak Ridge, TN 37831-9701, USA          |
  |      JULIA REWRITE: S. Patil, UW-Madison                                          |                                                                             |
  |      REFERENCES:                                                                  |
  |                                                                                   |
  |        1."COBRA: an optimized code for fast analysis of ideal ballooning         |
  |        stability of 3-D magnetic equilibria", R. Sanchez, S.P. Hirshman, J.C.     |
  |        Whitson and A.S. Ware, submitted to Journal of Computational Physics (1999)|
  |                                                                                   |
  |        2."Improved magnetic coordinate representation for ideal ballooning        |
  |        stability calculations with the COBRA code", R. Sanchez, S.P. Hirshman     |
  |        H.V. Wong, to be submitted to Journal of Computational physics (2000)      |
  |                                                                                   |
  |      DISCLAIMER:  This code is under development by R.Sanchez at the Departamento |
  |        de Fisica, Universidad Carlos III de Madrid, SPAIN. As a BETA version, the |
  |        code is supplied on "as it is" basis, and the non-existence of "bugs" of   |
  |        ANY kind is NOT guaranteed. Any problem or comment should be reported to   |
  |        R. Sanchez at raul.sanchez@uc3m.es                                         |
  _____________________________________________________________________________________
  =#
  (r0, amin, beta0, b0_v) = order_input(surf);

  params = BalloonParams(nwells, geom_input, tokamak_input, mode, r0, amin, beta0, b0_v)


  # Solve ballooning equation
  bgarray = zeros(length(θs), length(ζs))

  bci = FCInterp([],[],[],0,0)
  if interp
    bci = BallooningCoefInterp(surf, params; reltol, spline)
  end

  for (iθ, init_theta) in enumerate(θs)             # Loop over all poloidal, toroidal initial pairs
    for (iζ, init_zeta) in enumerate(ζs)
      #println("at: ",s," ",init_theta," ",init_zeta)

      # the following to be used as args in get_ballooning_grate
      init_zeta = mod(init_zeta, 2*π)/surf.nfp;
      init_theta = mod(init_theta, 2*π);

      # get growth rates on desired surfaces

      bg, xm = get_ballooning_grate(surf, init_zeta, init_theta, params, bci, interp, np);
      bgarray[iθ, iζ] = bg

    end
  end

  return bgarray
end


"""
    cobra_opt(eq, slist, nwells; θs, ζs, mode, geom_input, tokamak_input,
                   lscreen, print_progress, interp, reltol, return_all, return_locs)

Calculates locations of maximally unstable eigenvalues

# Arguments
 - `eq::E`: A Vmec or DESC equilibrium
 - `slist::AbstractVector`: list of s values
 - `nwells::Integer`: Number of helical wells to include
# Optional inputs
 - `θs::AbstractVector`: Vector of starting θ values (in radians)
 - `ζs::AbstractVector`: Vector of starting ζ values (in radians normalized to field periods)
 - `reltol::Number`: Relative tolerance input for coefficient interpolation
 - `np::Integer`: Number of points used in eigenvalue calculation
 - `print_progress::Bool`: Print every time a new s-surface is used (adds excitement)
 - `spline::Bool`: interpolate using splines
 -
"""
function cobra_opt(eq::E, slist::AbstractVector, nwells::Integer;
                   θs::AbstractVector = (0:3).*(2π/4),
                   ζs::AbstractVector = (0:3).*(2π/4),
                   print_progress::Bool=false, reltol::Number=1e-4,
                   np::Integer = 793, spline=true, return_all = false,
                   return_locs = false
                   ) where {E <: AbstractMagneticEquilibrium}

  # MAX: This should probably throw an error / warning instead of removing points
  slist = slist[slist .> 0.001]
  slist = slist[slist .< 0.999]
  ns = length(slist)
  nθ = length(θs)
  nζ = length(ζs)
  surf = magnetic_surface(slist[1], eq)
  #B0 = surf.B0 #need to figure out how to shortcut this for desc calls

  balloonGrowth = return_all ? zeros(ns, nθ, nζ) : zeros(ns)
  balloonGrowthLoc = return_locs ? zeros(2, ns, nθ, nζ) : nothing

  for (i_s,s) in enumerate(slist)
    if i_s > 1
   #   surf = magnetic_surface(s, eq, B0=B0)
      surf = magnetic_surface(s, eq)
    end
    bg = cobra_opt(surf, nwells, θs = θs, ζs = ζs, print_progress=print_progress,
                        reltol = reltol, np = np, spline=spline,
                        return_all = return_all, return_locs = return_locs)
    if return_locs && return_all
        balloonGrowth[i_s, :, :] = bg[1]
        balloonGrowthLoc[:,i_s,:,:] = bg[2]
    elseif !return_locs && return_all
        balloonGrowth[i_s, :, :] = bg
    else
        balloonGrowth[i_s] = bg
    end
  end
  if return_locs
    return balloonGrowth, balloonGrowthLoc
  else
    return balloonGrowth
  end
end

function cobra_opt(surf::S, nwells::Integer;
                   θs::AbstractVector = (0:3).*(2π/4),
                   ζs::AbstractVector = (0:3).*(2π/4),
                   print_progress::Bool=false, reltol::Number=1e-4,
                   np::Integer = 793, spline=true, return_all = false,
                   return_locs = false
                   ) where {S <: AbstractSurface}


  nθ = length(θs)
  nζ = length(ζs)

  # I'm pretty sure these flags are deprecated
  geom_input = true;
  tokamak_input = false;
  mode = 1;

  (r0, amin, beta0, b0_v) = order_input(surf);
  params = BalloonParams(nwells, geom_input, tokamak_input, mode, r0, amin, beta0, b0_v)
  μ0 = 4*π*1.0E-7
  # Interpolate the surface for coefficients
  #don't recompute the surface if not needed
  bci = BallooningCoefInterp(surf, params; reltol, spline)
  ι = get_iota(bci);

  if print_progress
    println("i_s = $(i_s)/$(length(slist)), ι = $(ι)"); flush(stdout);
  end

  # Get function that returns eigenvalue matrices for eig_opt
  xmax = π*(2*params.k_w - 1) ./ (2*surf.nfp*surf.iota[1])
  np  = Int(6 * floor(np / 6) + 1);      # number of points always odd and multiple of 3
  xmin = -xmax;
  h   = 2*(xmax/(np - 1));
  KMfun = getmatrix_function(xmin, h, np, bci)

  balloonGrowth = zeros(nθ, nζ)
  balloonGrowthLoc = zeros(2, nθ, nζ);

  # Loop over all poloidal, toroidal initial pairs
  for (iθ, init_theta) in enumerate(θs), (iζ, init_zeta) in enumerate(ζs)
    loc, λ = eig_opt(KMfun, [init_theta, init_zeta], surf.nfp)
    balloonGrowth[iθ, iζ] = λ;
    balloonGrowthLoc[:, iθ, iζ] = loc
  end

  if return_locs && return_all
    return balloonGrowth, balloonGrowthLoc
  elseif !return_locs && return_all
    return balloonGrowth
  else
    return maximum(balloonGrowth)
  end
end
